#ifndef PULSE_GEN_INCLUDE
#define PULSE_GEN_INCLUDE

double Box_Muller(); //generates normally distributed random numbers from uniform distribution
void gen_synth_pulse(int amp, int T0, double risingEdge, int length, short* wf); //generates a synthetic pulse shape where a semi-gaussian current pulse is integrated and run through a CR-(RC)^2 shaper
void gen_sim_pulse(int amp, int length, short *wf); //generates a simulated pulse shape based on MC output defining energy deposition profile for incident particles
void superimpose_noise(int length, short* wf, double stdDev); //adds noise to generated pulse as defined by data/power_spectrum.dat
void initialize_noise();

#endif
