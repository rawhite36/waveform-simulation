//This script generates a WF via Gaussian convolutions over the sum of an exponential
//and a step function. It then applies a noise spectrum obtained from a binary file in
//a loop such that the periodicity of local patterns is bLength and that of (global)
//WF patterns is ~bLength*LENGTH.
//The output for more than 2 pile-ups is undefined.



#ifndef wf_gen_C
#define wf_gen_C

#include "simulation.h"
#include <iostream>
#include <fstream>
#include <ctime>


// 6 arguments: WF amplitude (adc), number of WF's in each set, flag for synth. or sim.,
//							noise standard deviation (adc), delay time (ns), percent init. deposited
int main(int argc, char** argv)
{
  time_t timer;
  time(&timer);

  const short length = 3500;
  const short t0 = 1000;
  short* data[2];
  short ampl = 0;
	double risingEdge;
  int numb = 0;
	short stdDev;
	short delay;
	double percInit;
  std::string stemp;
  std::ofstream* fout;
  long long int WFID = 0;
  unsigned char result = 1;
  int other1 = 0;
  long long int other2 = 0;
  int wfLength = length;
	std::string outputFile{};

  if(argc==8)
  {
    initialize_noise();

    data[0] = new short[length];
    data[1] = new short[length];

    for(short i=0; argv[1][i]!='\0' && i<4; ++i)
      stemp += argv[1][i];

    ampl = stoi(stemp);
    stemp.clear();

    for(short i=0; argv[2][i]!='\0' && i<10; ++i)
      stemp += argv[2][i];

    risingEdge = stoi(stemp);
    stemp.clear();

    for(short i=0; argv[3][i]!='\0' && i<7; ++i)
      stemp += argv[3][i];

    numb = stoi(stemp);
    stemp.clear();

    for(short i=0; argv[4][i]!='\0' && i<4; ++i)
      stemp += argv[4][i];

    stdDev = stoi(stemp);
    stemp.clear();

    for(short i=0; argv[5][i]!='\0' && i<5; ++i)
      stemp += argv[5][i];

    delay = stoi(stemp);
    stemp.clear();

    for(short i=0; argv[6][i]!='\0' && i<100; ++i)
      stemp += argv[6][i];

    percInit = stod(stemp);

		for(short i{}; argv[7][i] != '\0' && i < 500; ++i)
			outputFile += {argv[7][i]};

    fout = new std::ofstream(outputFile, std::ios::binary);

    if(fout->good())
    {
      fout->write((char*)&WFID,8);

      for(int k=0; k<numb; ++k)
      {
				if(delay!=0)
				{
		      gen_synth_pulse(percInit*ampl,t0,risingEdge,length,data[0]);

		      for(short l=0; l<length; ++l)
		        data[1][l] = data[0][l];

		      gen_synth_pulse((1-percInit)*ampl,t0+delay/4,risingEdge,length,data[0]);

		      for(short l=0; l<length; ++l)
		        data[1][l] += data[0][l];
				}
				else
				{
		      gen_synth_pulse(ampl,t0,risingEdge,length,data[0]);

		      for(short l=0; l<length; ++l)
		        data[1][l] = data[0][l];
				}
        
        superimpose_noise(length,data[1],stdDev);

        fout->write((char*)&result, 1);
        fout->write((char*)&other1, 4);
        fout->write((char*)&other1, 4);
        fout->write((char*)&other1, 4);
        fout->write((char*)&other2, 8);
        fout->write((char*)&other2, 8);
        fout->write((char*)&wfLength, 4);
        fout->write((char*)data[1], 2*length);
      }
    }
    else
      std::cout << "\n Cannot Open File: " << stemp << "\n\n";

    delete fout;
    delete[] data[0];
    delete[] data[1];
  }
  else
    std::cout << "\n  3 arguments: WF amplitude, number of WF's in each set, and flag for "
            "synth. or sim.\n\n";

  std::cout << "\nTimer: " << difftime(time(nullptr),timer) << "\n\n";

  return 0;
}


#endif
